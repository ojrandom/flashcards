
<form id="flashcards-new-form" action="flashcards/{{$channel}}" method="post" class="acl-form" data-form_id="flashcards-new-form" data-allow_cid='{{$allow_cid}}' data-allow_gid='{{$allow_gid}}' data-deny_cid='{{$deny_cid}}' data-deny_gid='{{$deny_gid}}'>
    <button id="dbtn-acl" class="btn btn-outline-secondary btn-sm" type="button" data-toggle="modal" data-target="#aclModal" title="{{$perms_label}}"><i id="jot-perms-icon" class="fa fa-{{$lockstate}}"></i></button> Share
</form>
{{$aclselector}}
