# Diagrams to explain basic program flows

The diagrams where created with https://www.draw.io/

## Start of flashcards

![Start](img/start-flashcards.png)

## The spaced Repetition Learning Technique

### Classic Leitner System

This is the classical spaced repetition learning technique also know as [Leitner system](https://en.wikipedia.org/wiki/Spaced_repetition). 

![Start](img/learn-classic-flashcards.png)

### Modified Leitner System (default)

This is a slightly modified method adding some more repetition per deck. The default is three. The number of repetition can be configured. If you set the number of repetition to "1" this will result in the classic Leitner system.

![Start](img/learn-default-flashcards.png)

### Upload of flashcards

![Upload](img/upload-flashcards.png)
