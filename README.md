This plugin is a [flashcard software](https://en.wikipedia.org/wiki/List_of_flashcard_software) that uses [spaced repetition](https://en.wikipedia.org/wiki/Spaced_repetition) as a learning technique.

You can share the flash cards with other users of Hubzilla. Advantage of publicly shared cards: All parties will be able to add new cards and edit existing cards. The changes can be synchronised between each other.

Your learning progress will be kept private.

See the [Wiki](https://framagit.org/ojrandom/flashcards/wikis/home) for screenshots.

<img src="/addon/flashcards/view/img/leitner-system.png" align="center" width="70%"> 