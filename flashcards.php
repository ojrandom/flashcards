<?php

/**
 *
 * Name: FlashCards
 * Description: Hubzilla plugin for a flash card software that uses spaced repetition as a learning technique
 * Version: 0.0.3
 * Author: Tom Wiedenhöft (Einer von Vielen)
 * MinVersion: 3.4
 *
 */
define('ACTIVITY_OBJ_FLASHCARDS', NAMESPACE_ZOT . '/activity/flashcards');

/**
 *
 * @brief Return the current plugin version
 *
 * @return string Current plugin version
 */
function flashcards_get_version()
{
    return '0.0.3';
}

function flashcards_load()
{
    register_hook('load_pdl', 'addon/flashcards/flashcards.php', 'flashcards_load_pdl');
}

function flashcards_unload()
{
    unregister_hook('load_pdl', 'addon/flashcards/flashcards.php', 'flashcards_load_pdl');
}

function flashcards_install()
{
    info('FlashCards plugin installed successfully');
    logger('FlashCards plugin installed successfully');
}

function flashcards_uninstall()
{
    info('FlashCards plugin uninstalled successfully');
    logger('FlashCards plugin uninstalled successfully');
}

// Required in order for the plugin to return webpages at /flashcards as if it were
// a subfolder in /mod
function flashcards_module()
{
}

/**
 *
 * @brief Set the layout for page composition
 *
 * @return null
 */
function flashcards_load_pdl($a, &$b)
{
}

/**
 *
 * @brief Executes prior to page generation or $_REQUEST variables are parsed
 *
 * @return null
 */
function flashcards_init($a)
{
    $channel = App::get_channel();
    if (argc() === 1 && $channel['channel_address']) {
        goaway(z_root() . '/flashcards/' . $channel['channel_address']);
    }
}

/**
 *
 * @brief Outputs the main content of the page.
 * 
 * The browser will load the content of a box dynamically later on.
 * If the box is stored in the DB the client side javascript will redirect the
 * browser to a URL like
 * 
 *    my-domain.org/flashcards/my-channel-name/box-id
 * 
 * The user can use this URL to bookmark the box or share it with other users.
 * 
 * The user does not have to be authenticated.
 * The app can also be used "offline". How? The box content can be exported and
 * imported as JSON string under menu > "Local Backup"
 *
 * @return string HTML content
 */
function flashcards_content($a)
{
    head_add_css('/addon/flashcards/view/css/flashcards.css');
    if (!local_channel()) {
        notice(t('You must be logged in to see this page.') . EOL);
        return;
    }
    $channel = App::get_channel(); 
    $aclModal = flashcards_populate_acl($channel);
    $o = replace_macros(
            get_markup_template('flashcards.tpl', 'addon/flashcards'), array(
					'$channel' => $channel['channel_address'],     
                    '$version' => 'v' . flashcards_get_version(),
                    '$aclModal' => $aclModal
    ));
    return $o;
}

/**
 *
 * @brief This function provides the API endpoints, primarily called by the
 * JavaScript functions via $.post() calls.
 *
 * @return JSON-formatted structures with a "status" indicator for success
 *         as well as other requested data as the cards of a box
 */
function flashcards_post(&$a)
{
    $channel = App::get_channel();    
    if (!local_channel()) {
        logger('Must be local channel.', LOGGER_DEBUG);
        notice(t('Must be local channel.') . EOL);
        json_return_and_die(array('errormsg' => 'Must be local channel.', 'status' => false));
    }           
    if (argc() > 1) {
        switch (argv(1)) {
            case 'upload':
                // API: /flashcards/upload
                // Updates a box specified by param "box_id" or creates one if "box_id" not found
                flashcards_post_upload($channel);
            case 'download':
                // API: /flashcards/download
                // Downloads a box specified by param "box_id"
                flashcards_post_download($channel);
            case 'list':
                // API: /flashcards/list
                // List all boxes owned by the channel
                flashcards_post_list($channel);
            case 'delete':
                // API: /flashcards/delete
                // Deletes a box specified by param "box_id"
                flashcards_post_delete($channel);
            default:
                break;
        }
    }
}

function flashcards_post_upload($channel) {    
    $boxRemote = $_POST['box'];
    $box_id = $boxRemote["boxID"];
    $title = $boxRemote["title"]; // variable used for logging only

    $cards = $boxRemote['cards'];
    $cardIDsReceived = array();
    if(isset($cards)) {                    
        foreach ($cards as &$card) {
            array_push($cardIDsReceived, $card['content'][0]);
        }
    }
    
    $acl = new Zotlabs\Access\AccessList($channel);
    $acl->set_from_array($_REQUEST);
    
    if(strlen($box_id) > 0) {
        flashcards_post_merge_box($box_id, $channel, $boxRemote, $acl);
    } else {
        $b = flashcards_create_box($channel, $acl, $boxRemote);
        if ($b['status']) {
            $resource_id = $b['item']['obj']['boxID'];
            logger("Created new box of flashards. Title: " . $title . ", resource_id: " . $resource_id, LOGGER_DEBUG);
            json_return_and_die(array('status' => true, 'resource_id' => $resource_id, 'cardIDsReceived' => $cardIDsReceived));
        } else {
            notice(t('Error creating new box of flash cards.') . EOL);
            json_return_and_die(array('errormsg' => 'Error saving box', 'status' => false));
        }    
    }
}

function flashcards_post_merge_box($box_id, $channel, $boxRemote, $acl) {
    $b = flashcards_get_box($box_id);
    if (!$b['status']) {
        notice(t('Error saving box. Box not found.') . EOL);
        json_return_and_die(array('errormsg' => 'Error saving box. Box not found.', 'status' => false));
    }
    if ($b['item']['owner_xchan'] !== $channel['channel_hash']) {
        // TODO
        // - copy the box
        // - remove learn progress
        // - store in DB
        notice(t('Not implemented yet. You are not the owner of this box.') . EOL);
        json_return_and_die(array('errormsg' => 'Not implemented yet. You are not the owner of this box.', 'status' => false));
    }
    // Merge boxes
    $boxDB = json_decode($b['box'], true);
    $boxes = flashcards_merge($boxDB, $boxRemote);
    $boxToWrite = $boxes['boxDB'];
    $boxToSend = $boxes['boxRemote'];
    // Write the received box to DB
    $result = flashcards_update_box($b['item'], $boxToWrite, $acl);
    if(! $result['status']) {
        notice(t('DB update of box failed.') . EOL);
        json_return_and_die(array('errormsg' => 'DB update of box failed.', 'status' => false));
    }
    // Send box from DB to client. The box includes only cards that have changes or are new.
    logger("Updated box of flashards. Title: " . $title . ", resource_id: " . $box_id, LOGGER_DEBUG);
    json_return_and_die(array('status' => true, 'box' => $boxToSend, 'resource_id' => $box_id, 'cardIDsReceived' => $cardIDsReceived));
}

function flashcards_post_download($channel) {    
    $box_id = isset($_POST['boxID']) ? $_POST['boxID'] : ''; 
    if(strlen($box_id) > 0) {
        $b = flashcards_get_box($box_id);
        if (!$b['status']) {
            notice(t('Error downloading box. Box not found.') . EOL);
            json_return_and_die(array('errormsg' => 'Error downloading box. Box not found.', 'status' => false));
        }
        if ($b['item']['owner_xchan'] !== $channel['channel_hash']) {
            // TODO
            // - remove learn progress
            notice(t('Not implemented yet. You are not the owner of this box.') . EOL);
            json_return_and_die(array('errormsg' => 'Not implemented yet. You are not the owner of this box.', 'status' => false));
        }
        $box = json_decode($b['box'], true);
        $o = flashcards_populate_acl($channel, $b);
        logger("Sending box of flashards for download resource_id: " . $box_id, LOGGER_DEBUG);
        json_return_and_die(
                array(
                    'status' => true,
                    'box' => $box,
                    'resource_id' => $box_id,
                    'boxACL' => $o));
    }
}

function flashcards_populate_acl($channel, $b) {
    $acl = new \Zotlabs\Access\AccessList(false);
    if($b) {
        $acl->set($b['item']);        
    }
    $box_acl = $acl->get();
    require_once('include/acl_selectors.php');
    $aclselector = populate_acl($box_acl, false);
    $o = replace_macros(
            get_markup_template('flashcards_acl.tpl', 'addon/flashcards'), array(
                    '$aclselector' => $aclselector,
                    '$allow_cid' => acl2json($box_acl['allow_cid']),
                    '$allow_gid' => acl2json($box_acl['allow_gid']),
                    '$deny_cid' => acl2json($box_acl['deny_cid']),
                    '$deny_gid' => acl2json($box_acl['deny_gid']),
                    '$channel' => $channel['channel_address'],                    
                    '$perms_label' => t('Permission settings'),
                    '$lockstate' => (($acl->is_private()) ? 'lock' : 'unlock')
    ));
    return $o;
}

function flashcards_post_list($channel) {    
    logger('About to list boxes...', LOGGER_DEBUG);
    $boxes = get_flashcards_boxes();
    if (!$boxes['status']) {
        json_return_and_die(array('errormsg' => 'Error reading boxes from DB', 'status' => false));
    } else {
        json_return_and_die(array('status' => true, 'boxes_owned' => $boxes['boxes_owned'], 'boxes_not_owned' => $boxes['boxes_not_owned']));
    }
}

function flashcards_post_delete($channel) {    
    $box_id = isset($_POST['boxID']) ? $_POST['boxID'] : ''; 
    $d = flashcards_delete_box($box_id, $channel);
    if (!$d['status']) {
        notice(t('Error deleting box') . EOL);
        json_return_and_die(array('errormsg' => 'Error deleting box', 'status' => false));
    } else {
        json_return_and_die(array('status' => true));
    }
}

/**
 *
 * @brief Create a box of flash cards by generating a new item table record as a standard
 * post. This will post a message.
 *
 * @return array Status and parameters of the new box post
 */
function flashcards_create_box($channel, $acl, $box)
{
    $resource_type = 'flashcards';
    // Generate unique resource_id using the same method as item_message_id()
    do {
        $dups = false;
        $resource_id = random_string(5);
        $r = q("SELECT mid FROM item WHERE resource_id = '%s' AND resource_type = '%s' AND uid = %d LIMIT 1",
            dbesc($resource_id),
            dbesc($resource_type),
            intval($channel['channel_id']));
        if (count($r))
            $dups = true;
    } while ($dups == true);
    $ac = $acl->get();
    $mid = item_message_id();
    $arr = array(); // Initialize the array of parameters for the post
    $item_hidden = 0; // TODO: Allow form creator to send post to ACL about new box automatically
    $box_url = z_root() . '/flashcards/' . $channel['channel_address'] . '/' . $resource_id;
    $box['boxID'] = $resource_id;
    $box['url'] = $box_url;
    $arr['aid'] = $channel['channel_account_id'];
    $arr['uid'] = $channel['channel_id'];
    $arr['mid'] = $mid;
    $arr['parent_mid'] = $mid;
    $arr['item_hidden'] = $item_hidden;
    $arr['resource_type'] = $resource_type;
    $arr['resource_id'] = $resource_id;
    $arr['owner_xchan'] = $channel['channel_hash'];
    $arr['author_xchan'] = $channel['channel_hash'];
    $arr['title'] = 'box';
    $arr['allow_cid'] = $ac['allow_cid'];
    $arr['allow_gid'] = $ac['allow_gid'];
    $arr['deny_cid'] = $ac['deny_cid'];
    $arr['deny_gid'] = $ac['deny_gid'];
    $arr['item_wall'] = 1;
    $arr['item_origin'] = 1;
    $arr['item_thread_top'] = 1;
    $arr['item_private'] = intval($acl->is_private());
    $arr['verb'] = ACTIVITY_CREATE;
    $arr['obj_type'] = ACTIVITY_OBJ_FLASHCARDS;
    $arr['obj'] = $box;
    unset($arr['obj']['cards']); // remove the cards
    $arr['body'] = 'Created a new box of flash cards. Titel: ' . $box["title"] . ', box URL: ' . $box_url;
    
    $object = json_encode($box);
    // Hint: set_iconfig(...) will store the id of the item as iid in iconfig.
    // Later on the iid will be used to find the iconfig for an item stored in table item. 
    if(! set_iconfig($arr, 'flashcards', 'box', $object, true)) {
        return array('item' => null, 'status' => false);
    }
    $post = item_store($arr);
    $item_id = $post['item_id'];
    
    if ($item_id) {
        Zotlabs\Daemon\Master::Summon([
            'Notifier',
            'activity',
            $item_id
        ]);
        return array(
            'item' => $arr,
            'status' => true
        );
    } else {
        return array(
            'item' => null,
            'status' => false
        );
    }
    
}

/**
 * @brief Retrieve the game item data structure
 *
 * @param $box_id String unique box ID string
 * @return array Success of retrieval and box item
 */
function flashcards_get_box($box_id) {
    $item = q("SELECT * FROM item "
        . "WHERE resource_id = '%s' "
        . "AND resource_type = '%s' "
        . "AND mid = parent_mid AND item_deleted = 0 LIMIT 1",
        dbesc($box_id),
        dbesc('flashcards')
        );
    if (!$item) {
        return array('box' => null, 'status' => false);
    } else {        
        $b = $item[0];	// box item table record
        $box  = get_iconfig($b, 'flashcards', 'box');
        if (!$box) {
            return array('box' => null, 'status' => false);
        } else {
            return array('box' => $box, 'status'  => true, 'item' => $b);
        }
    }
}

/**
 * @brief Updates the box item with the latest data
 *
 * @param $item Array box as item in DB
 * @param $boxContent String content of a box
 * @return array Success of box item update
 */
function flashcards_update_box($item, $boxContent, $acl) {
    $copy = $boxContent;
    unset($copy['cards']); // remove the cards
    $item['obj'] = json_encode($copy);   
    
    $ac = $acl->get();
    $item['allow_cid'] = $ac['allow_cid'];
    $item['allow_gid'] = $ac['allow_gid'];
    $item['deny_cid'] = $ac['deny_cid'];
    $item['deny_gid'] = $ac['deny_gid'];
    $item['item_private'] = intval($acl->is_private());
    
    $object = json_encode($boxContent);
    // Hint: This adds the box as iconfig to the item array.
    // It does NOT store the box in the DB.
    if(! set_iconfig($item, 'flashcards', 'box', $object, true)) {
        return array('status'  => false);;
    }
    // Hint: This updates the iconfig implicitly, more precise it first deletes
    // and then inserts the box into table iconfig
    $ret = item_store_update($item);
    return array('status'  => $ret['success']);
//    $boxobj = json_encode($box);
//    $r = q("UPDATE item set obj = '%s' WHERE mid = '%s' AND resource_type = '%s'",
//        dbesc($boxobj),
//        dbesc($mid),
//        dbesc('flashcards')
//        );
}

/**
 * @brief Delete a box using the standard drop_item method for posts in the
 * item table. The box content itself is stored in table iconfig and therefor
 * not deleted.
 *
 * @param $box_id String ID of the game to be deleted
 * @param $channel Array authenticated local channel requesting the deletion
 * @return array Success of deletion and item that was deleted
 */
function flashcards_delete_box($box_id, $channel) {
    logger('Select items with boxID = .' . $box_id, LOGGER_DEBUG);
    $items = q("SELECT id FROM item WHERE resource_type = '%s' AND resource_id = '%s' "
        . "AND uid = %d AND item_deleted = 0 limit 1",
        dbesc('flashcards'),
        dbesc($box_id),
        intval($channel['channel_id'])
        );
    if (!$items) {
        logger('No item(s) found for boxID = .' . $box_id, LOGGER_DEBUG);
        return array('items' => null, 'status' => false);
    } else {
        logger('About to drop item = .' . $items[0]['id'], LOGGER_DEBUG);
        $drop = drop_item($items[0]['id'], false, DROPITEM_NORMAL, true);
        return array('items' => $items, 'status' => (($drop === 1) ? true : false));
    }
}

/**
 *
 * @brief Retrieve a list of boxes in separate lists of those owned and those not owned
 * 
 * @return array boxes owned and not owned
 */
function get_flashcards_boxes()
{
    $channel = App::get_channel();
    $owner_hash = $channel['channel_hash'];
    
    // own boxes
    logger('Find boxes owned by requester in DB...', LOGGER_DEBUG);
    $boxes_owned = q("SELECT * FROM item "
        . "WHERE resource_type = '%s' "
        . "AND title = '%s' "
        . "AND owner_xchan = '%s' "
        . "AND item_deleted = 0 order by id desc",
        dbesc('flashcards'),
        dbesc('box'),
        dbesc($owner_hash));
    $boxes_owned_min = [];
    foreach ($boxes_owned as $box) {
        $boxobj = json_decode($box['obj'], true);
        if(!$boxobj) {
            // This if can be deleted if the plugin is published
            logger('Found box that is NULL. This could have happened in an alpha version of this software.', LOGGER_DEBUG);
            continue;
        }
        $boxes_owned_min[] = $boxobj;
        logger("Found box id = " . $boxobj["boxID"] . ", title = " . $boxobj["title"], LOGGER_DEBUG);
    }
    
    // not own boxes
    logger('Find boxes not owned by requester in DB...');
    $boxes_not_owned = q("SELECT * FROM item "
        . "WHERE resource_type = '%s' "
        . "AND title = '%s' "
        . "AND owner_xchan <> '%s' "
        . "AND item_deleted = 0 order by id desc",
        dbesc('flashcards'),
        dbesc('box'),
        dbesc($owner_hash));
    $boxes_not_owned_min = [];
    foreach ($boxes_not_owned as $box) {
        $boxobj = json_decode($box['obj'], true);
        if(!$boxobj) {
            // This if can be deleted if the plugin is published
            logger('Found box that is NULL. This could have happened in an alpha version of this software.', LOGGER_DEBUG);
            continue;
        }
        $boxes_not_owned_min[] = $boxobj;
        logger('Found box id = ' . $boxobj['boxID'] . ', title = ' . $boxobj['title'], LOGGER_DEBUG);
    }
    
    return array(
        'status' => true,
        'boxes_owned' => $boxes_owned_min,
        'boxes_not_owned' => $boxes_not_owned_min);
}

/*
 * Merge to boxes of flashcards
 * 
 *  compare
 *  - publicId > if not equal then do nothing
 *  do not touch
 *  - boxID
 *  - creator
 *  - lastShared
 *  - maxLengthCardField
 *  - cardsColumnName
 *  - private_hasChanged
 *  lastChangedPublicMetaData
 *  - title
 *  - description
 *  - lastEditor
 *  lastChangedPrivateMetaData
 *  - cardsDecks
 *  - cardsDeckWaitExponent
 *  - cardsRepetitionsPerDeck
 *  - private_sortColumn
 *  - private_sortReverse
 *  - private_filter
 *  - private_visibleColumns
 *  - private_switch_learn_direction
 *  - private_switch_learn_all
 *  calculate
 *  - size
 *  cards
 *  0 - id = creation timestamp, milliseconds, Integer > do not touch
 *  1 - Language A, String > last modified content
 *  2 - Language B, String > last modified content
 *  3 - Description, String > last modified content
 *  4 - Tags, "Lesson 010.03" or anything else, String > last modified content
 *  5 - last modified content, milliseconds, Integer
 *  6 - Deck, Integer from 0 to 6 but configurable > last modified progress
 *  7 - progress in deck default 0, Integer > last modified progress
 *  8 - How often learned (information for the user only), Integer > last modified progress
 *  9 - last modified progress, milliseconds, Integer
 *  10 - has local changes, Boolean > use to create new box to send
 *  
 * @param $boxDB array from local DB
 * @param $boxRemote array received to merge with box in DB
 */
function flashcards_merge($boxDB, $boxRemote) {
    if($boxDB['publicId'] !== $boxRemote['publicId']) {
        return NULL;
    }
    $keysPublic = array('title', 'description', 'lastEditor', 'lastChangedPublicMetaData', 'lastShared');
    $keysPrivate = array('cardsDecks', 'cardsDeckWaitExponent', 'cardsRepetitionsPerDeck', 'private_sortColumn', 'private_sortReverse', 'private_filter', 'private_visibleColumns', 'private_switch_learn_direction', 'private_switch_learn_all', 'lastChangedPrivateMetaData');
    if($boxDB['lastChangedPublicMetaData'] !== $boxRemote['lastChangedPublicMetaData']) {
        if($boxDB['lastChangedPublicMetaData'] > $boxRemote['lastChangedPublicMetaData']) {
            foreach ($keysPublic as &$key) {
                $boxRemote[$key] = $boxDB[$key];
            }
        } else {
            foreach ($keysPublic as &$key) {
                $boxDB[$key] = $boxRemote[$key];
            }
        }
    }
    if($boxDB['lastChangedPrivateMetaData'] !== $boxRemote['lastChangedPrivateMetaData']) {
        if($boxDB['lastChangedPrivateMetaData'] > $boxRemote['lastChangedPrivateMetaData']) {
            foreach ($keysPrivate as &$key) {
                $boxRemote[$key] = $boxDB[$key];
            }
        } else {
            foreach ($keysPrivate as &$key) {
                $boxDB[$key] = $boxRemote[$key];
            }
        }
    }
    $cardsDB = $boxDB['cards'];
    $cardsRemote = $boxRemote['cards'];
    $cardsDBadded = array();
    $cardsRemoteToUpload = array();
    foreach ($cardsRemote as &$cardRemote) {
        $isInDB = false;
        foreach ($cardsDB as &$cardDB) {
            if($cardRemote['content'][0] === $cardDB['content'][0]) {
                $isInDB = true;
                $isRemoteChanged = false;
                if($cardDB['content'][5] !== $cardRemote['content'][5]) {
                    if($cardDB['content'][5] > $cardRemote['content'][5]) {
                        for ($i = 1; $i < 6; $i++) {
                            $cardRemote['content'][$i] = $cardDB['content'][$i];
                            $isRemoteChanged = true;
                        }
                    } else {
                        for ($i = 1; $i < 6; $i++) {
                            $cardDB['content'][$i] = $cardRemote['content'][$i];
                        }
                    }
                }
                if($cardDB['content'][9] !== $cardRemote['content'][9]) {
                    if($cardDB['content'][9] > $cardRemote['content'][9]) {
                        for ($i = 6; $i < 10; $i++) {
                            $cardRemote['content'][$i] = $cardDB['content'][$i];
                            $isRemoteChanged = true;
                        }
                    } else {
                        for ($i = 6; $i < 10; $i++) {
                            $cardDB['content'][$i] = $cardRemote['content'][$i];
                        }
                    }
                }
                if($isRemoteChanged === true) {
                    array_push($cardsRemoteToUpload, $cardDB);
                }
                break;
            }
        }
        if(!$isInDB) {
            array_push($cardsDBadded, $cardRemote);
        }
    }
    // Add cards from local DB that are not in the remote cards
    foreach ($cardsDB as &$cardDB) {
        $isInRemote = false;
        foreach ($cardsRemote as &$cardRemote) {
            if($cardRemote[0] === $cardDB[0]) {
                $isInRemote = true;
                break;
            }
        }
        if(!$isInRemote) {
            array_push($cardsRemoteToUpload, $cardDB);
        }
    }
    $cardsDB = array_merge($cardsDB, $cardsDBadded);
    $boxDB['size'] = count($cardsDB);
    $boxRemote['size'] = count($cardsDB);
    $boxDB['cards'] = $cardsDB;
    $boxRemote['cards'] = $cardsRemoteToUpload; // send changed or new cards only
    return array('boxDB' => $boxDB, 'boxRemote' => $boxRemote);
}
function flashcards_merge_test() {
    $boxIn1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[]}';
    $boxIn2 = '{"boxID":"b2b2b2","title":"a2aaaaaaaaaaa","description":"A2aaaaaaaaaaaa","creator":"bMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"dMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"6","cardsDeckWaitExponent":"2","cardsRepetitionsPerDeck":"1","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":1,"private_sortReverse":false,"private_filter":["b","","","","","","","","","",""],"private_visibleColumns":[true,false,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":true,"private_switch_learn_all":true,"private_hasChanged":false,"cards":[]}';
    $box1 = json_decode($boxIn1, true);
    $box2 = json_decode($boxIn2, true);
    // public id does not match
    $box2['publicId'] = 'b';
    $boxes = flashcards_merge($box1, $box2);
    if($boxes) {
        return;
    }
    // Nothing changed
    $box2['publicId'] = $box1['publicId'];
    $boxes = flashcards_merge($box1, $box2);
    $boxOut1 = json_encode($box1);
    $boxOut2 = json_encode($box2);
    if($boxOut1 !== $boxIn1) {
        return;
    }
    if($boxOut2 !== $boxIn2) {
        return;
    }
    // Public and private meta data
    $boxIn1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[]}';
    $boxIn2 = '{"boxID":"b2b2b2","title":"a2aaaaaaaaaaa","description":"A2aaaaaaaaaaaa","creator":"bMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"dMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"6","cardsDeckWaitExponent":"2","cardsRepetitionsPerDeck":"1","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":1,"private_sortReverse":false,"private_filter":["b","","","","","","","","","",""],"private_visibleColumns":[true,false,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":true,"private_switch_learn_all":true,"private_hasChanged":false,"cards":[]}';
    $box1 = json_decode($boxIn1, true);
    $box2 = json_decode($boxIn2, true);
    $boxCompare1 = '{"boxID":"a1a1a1","title":"a2aaaaaaaaaaa","description":"A2aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"dMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[]}';
    $boxCompare2 = '{"boxID":"b2b2b2","title":"a2aaaaaaaaaaa","description":"A2aaaaaaaaaaaa","creator":"bMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"dMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":false,"cards":[]}';
    $boxes = flashcards_merge($box1, $box2);
    $boxOut1 = json_encode($boxes['boxDB']);
    $boxOut2 = json_encode($boxes['boxRemote']);
    if($boxOut1 !== $boxCompare1) {
        return;
    }
    if($boxOut2 !== $boxCompare2) {
        return;
    }
    // Public and private meta data the other way around
    $boxIn1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[]}';
    $boxIn2 = '{"boxID":"b2b2b2","title":"a2aaaaaaaaaaa","description":"A2aaaaaaaaaaaa","creator":"bMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"dMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"6","cardsDeckWaitExponent":"2","cardsRepetitionsPerDeck":"1","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":1,"private_sortReverse":true,"private_filter":["b","","","","","","","","","",""],"private_visibleColumns":[true,false,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":true,"private_switch_learn_all":true,"private_hasChanged":false,"cards":[]}';
    $box1 = json_decode($boxIn1, true);
    $box2 = json_decode($boxIn2, true);
    $boxCompare1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"6","cardsDeckWaitExponent":"2","cardsRepetitionsPerDeck":"1","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":1,"private_sortReverse":true,"private_filter":["b","","","","","","","","","",""],"private_visibleColumns":[true,false,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":true,"private_switch_learn_all":true,"private_hasChanged":true,"cards":[]}';
    $boxCompare2 = '{"boxID":"b2b2b2","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"bMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219599,"maxLengthCardField":1000,"cardsDecks":"6","cardsDeckWaitExponent":"2","cardsRepetitionsPerDeck":"1","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219599,"private_sortColumn":1,"private_sortReverse":true,"private_filter":["b","","","","","","","","","",""],"private_visibleColumns":[true,false,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":true,"private_switch_learn_all":true,"private_hasChanged":false,"cards":[]}';
    $boxes = flashcards_merge($box1, $box2);
    $boxOut1 = json_encode($boxes['boxDB']);
    $boxOut2 = json_encode($boxes['boxRemote']);
    if($boxOut1 !== $boxCompare1) {
        return;
    }
    if($boxOut2 !== $boxCompare2) {
        return;
    }
    // Add remote card to empty local cards
    $boxIn1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[]}';
    $boxIn2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230161,0,0,0,0,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $box1 = json_decode($boxIn1, true);
    $box2 = json_decode($boxIn2, true);
    $boxCompare1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230161,0,0,0,0,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxCompare2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[]}';
    $boxes = flashcards_merge($box1, $box2);
    $boxOut1 = json_encode($boxes['boxDB']);
    $boxOut2 = json_encode($boxes['boxRemote']);
    if($boxOut1 !== $boxCompare1) {
        return;
    }
    if($boxOut2 !== $boxCompare2) {
        return;
    }
    // Add local cards to empty remote cards
    $boxIn1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230161,0,0,0,0,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxIn2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[]}';
    $box1 = json_decode($boxIn1, true);
    $box2 = json_decode($boxIn2, true);
    $boxCompare1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230161,0,0,0,0,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxCompare2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230161,0,0,0,0,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxes = flashcards_merge($box1, $box2);
    $boxOut1 = json_encode($boxes['boxDB']);
    $boxOut2 = json_encode($boxes['boxRemote']);
    if($boxOut1 !== $boxCompare1) {
        return;
    }
    if($boxOut2 !== $boxCompare2) {
        return;
    }
    // change card values
    $boxIn1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"ax","aax","aaax","aaaax",1531058230161,0,0,0,1531058230162,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxIn2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230162,1,2,3,1531058230161,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $box1 = json_decode($boxIn1, true);
    $box2 = json_decode($boxIn2, true);
    $boxCompare1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230162,0,0,0,1531058230162,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxCompare2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230162,0,0,0,1531058230162,true]}]}';
    $boxes = flashcards_merge($box1, $box2);
    $boxOut1 = json_encode($boxes['boxDB']);
    $boxOut2 = json_encode($boxes['boxRemote']);
    if($boxOut1 !== $boxCompare1) {
        return;
    }
    if($boxOut2 !== $boxCompare2) {
        return;
    }
    // change card values the other way around
    $boxIn1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230162,1,2,3,1531058230161,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxIn2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"ax","aax","aaax","aaaax",1531058230161,0,0,0,1531058230162,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $box1 = json_decode($boxIn1, true);
    $box2 = json_decode($boxIn2, true);
    $boxCompare1 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230162,0,0,0,1531058230162,true]},{"content":[1531058231082,"b1","b1b","b1bb","b1bbb",1531058239161,2,4,6,8,true]}]}';
    $boxCompare2 = '{"boxID":"a1a1a1","title":"a1aaaaaaaaaaa","description":"A1aaaaaaaaaaaa","creator":"aMan","lastShared":0,"publicId":1531058219599,"size":2,"lastEditor":"cMan","lastChangedPublicMetaData":1531058219598,"maxLengthCardField":1000,"cardsDecks":"7","cardsDeckWaitExponent":"3","cardsRepetitionsPerDeck":"3","cardsColumnName":["Created","Side 1","Side 2","Description","Tags","modified","Deck","Progress","Counter","Learnt","Upload"],"lastChangedPrivateMetaData":1531058219598,"private_sortColumn":0,"private_sortReverse":false,"private_filter":["a","","","","","","","","","",""],"private_visibleColumns":[false,true,true,true,true,false,false,false,false,false,false],"private_switch_learn_direction":false,"private_switch_learn_all":false,"private_hasChanged":true,"cards":[{"content":[1531058221298,"a","aa","aaa","aaaa",1531058230162,0,0,0,1531058230162,true]}]}';
    $boxes = flashcards_merge($box1, $box2);
    $boxOut1 = json_encode($boxes['boxDB']);
    $boxOut2 = json_encode($boxes['boxRemote']);
    if($boxOut1 !== $boxCompare1) {
        return;
    }
    if($boxOut2 !== $boxCompare2) {
        return;
    }
    logger('tests all passed');
}

